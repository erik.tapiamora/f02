const util = require('util')
class Checklist {
    constructor(groups = []) {
        this.groups = groups;
    }

    add(title,subGroup,checks){        
        this.groups.push(new Group(title,subGroup,checks));
        return this;
    }


}

class Group {
    constructor (title, subGroups = [], checks = []) {
        this.title = title;
        this.subGroup = subGroups;
        this.checks = checks;
    }
    


    addSubGroup(title,subGroups,checks){        
        this.subGroup.push(new SubGroup(title,subGroups,checks));
        return this;
    }

    addSubGroup(subgroup){        
        this.subGroup.push(subgroup);
        return this;
    }

    addCheck(name,value){        
        this.checks.push(new Check(name,value));
        return this;
    }
}

class SubGroup {
    constructor(title, subGroups = [], checks = []) {
        this.title = title;
        this.subGroups = subGroups;
        this.checks = checks;
    }

    addSubGroup(title,subgroups,checks){        
        this.subGroups.push(title,subgroups,checks);
        return this;
    }

    addSubGroup(subgroup){        
        this.subGroups.push(subgroup);
        return this;
    }

    addCheck(check){        
        this.checks.push(check); 
        return this;
    }
}

class Check {
    constructor(name, value = 0) {
        this.name = name;
        this.value = value;
    }

    setValue(name,value){
        this.name=name;
        this.value = value;
        return this;
    }
}


let checklist = new Checklist();
let grupo = new Group('Principal');
let subGrupo = new SubGroup('Subgrupo 1');
let subgrupo2 = new SubGroup('Subgrupo 2');
let subgrupo3 = new SubGroup('Subgrupo 3');
let campo = new Check('Tarea','Iniciado');



checklist.add('Lista principal',grupo.addSubGroup(subGrupo.addSubGroup(subgrupo2.addSubGroup(subgrupo3.addCheck(campo)))));


console.log(util.inspect(grupo, false, null, true ))

